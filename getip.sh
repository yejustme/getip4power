#/bin/bash
##必须使用绝对路径
#必须首先初始化git仓库：git remote add origin git@gitlab.com:yejustme/getip4power.git
#进行一次提交命令：git commit -m "update ip"
#执行一次./getip.sh
#使用crontab -e命令添加如下项目，则每小时的10自动运行，使用绝对路径（不含#）：
#10 *  *  * * /home/power/mydata/MyCode/myutils/getip4power/getip.sh
################parameters################
thisdir='/home/power/mydata/MyCode/myutils/getip4power/'
device='enp4s0'
##########################################
main(){
local thisdir=$1   #must use the absolute path:
local device=$2
local file='ip.txt'

cd $thisdir

if [ ! -f $file  ];then
	touch ip.txt
fi

mac=$(ip addr ls $device |grep ether)
ipv4=$(ip -4 addr ls $device | grep inet)
ipv6=$(ip -6 addr ls $device | grep inet6)
content=$(date)"\n"Mac"\n"$mac"\n"ipv4"\n"$ipv4"\n"ipv6"\n"$ipv6
echo -e $content>ip.txt

git add .
git commit --amend -m "update ip"
git push --force origin master
#use crontab to autorun this  .sh file.
}

main $thisdir $device
